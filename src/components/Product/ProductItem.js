import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class ProductItem extends Component {

    onDelete = (id) => {
        if(confirm('Bạn muốn xóa !')){ // eslint-disable-line
            this.props.onDelete(id);
        }
    }

    render() {
        var {product, index} = this.props;
        var statusName = product.status ? 'Con Hang' : 'Het Hang';
        var statusClass = product.status ? 'success' : 'danger';
        return (
            <tr>
                <td>{index + 1}</td>
                <td>{product.id}</td>
                <td>{product.name}</td>
                <td>{product.price}</td>
                <td><span className={`label label-${statusClass}`}> {statusName} </span></td>
                <td>
                    <Link to={`/product/edit/${product.id}`} className="btn btn-warning">Sửa</Link>
                     - 
                     <button type="button" className="btn btn-danger" onClick={() => this.onDelete(product.id)}>Xóa</button>
                </td>
            </tr>
        );
    }
}

export default ProductItem;
