import * as types from './../constants/ActionType';
import callApi from './../utils/apiCaller';


export const actionGetDetailProductRequest = (id) => {
    return (dispatch) => {
        return callApi(`products/${id}`)
            .then(response => {
                dispatch(actionGetDetailProduct(response.data));
            })
    }
}

export const actionGetDetailProduct = (products) => {
    return {
        type: types.GET_DETAIL_PRODUCTS,
        products
    }
}

export const actionGetProductRequest = () => {
    return (dispatch) => {
        return callApi('products')
            .then(response => {
                dispatch(actionGetProducts(response.data));
            })
    }
}

export const actionGetProducts = (products) => {
    return {
        type: types.GET_PRODUCTS,
        products
    }
}

export const actionDeleteProductsRequest = (id) => {
    return (dispatch) => {
        return callApi(`products/${id}`, 'DELETE')
            .then(response => {
                dispatch(actionDeleteProducts(id));
            })
    }
}

export const actionDeleteProducts = (id) => {
    return {
        type: types.REMOVE_PRODUCTS,
        id
    }
}

export const actionAddProductsRequest = (body) => {
    return (dispatch) => {
        return callApi('products', 'POST', body)
            .then(response => {
                if (response.status === 201) {
                    dispatch(actionAddProducts(response.data))
                }
            });
    }
}

export const actionAddProducts = (products) => {
    return {
        type: types.ADD_PRODUCTS,
        products
    }
}

export const actionUpdateProductsRequest = (body) => {
    return (dispatch) => {
        return callApi(`products/${body.id}`, 'PUT', body)
            .then(response => {
                if (response.status === 200) {
                    dispatch(actionUpdateProducts(response.data))
                }
            });
    }
}

export const actionUpdateProducts = (products) => {
    return {
        type: types.UPDATE_PRODUCTS,
        products
    }
}
