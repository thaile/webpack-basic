export default function findIndexInArray(array, id){
    var index = -1;
    if(array.length > 0){
        for(var i=0; i < array.length; i++){
            if(array[i].id === id){
                index = i;
                break;
            }
        }
    }
    return index;
}