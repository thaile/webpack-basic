import axios from 'axios';
import { URL_API } from './../constants/Config';

export default function callApi(endpoint, method = 'GET', data = null) {
    return axios({
        method: method,
        url: `${URL_API}/${endpoint}`,
        data: data
    })
        .catch(function (error) {
            console.log(error);
        });
}