import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actionAddProductsRequest, actionGetDetailProductRequest, actionUpdateProductsRequest } from './../../actions/index';

class ProductActionPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            price: '',
            status: ''
        }
    }

    componentDidMount() {
        var { match } = this.props;
        if (match) {
            var id = match.params.id;
            this.props.getProductItem(id);
        }
    }

    componentWillReceiveProps(nextProps){
        if(nextProps && nextProps.productItem){
            var {productItem} = nextProps;
            this.setState({
                id: productItem.id,
                name: productItem.name,
                price: productItem.price,
                status: productItem.status
            });
        }
        
    }

    onChange = e => {
        var target = e.target;
        var name = target.name;
        var value = target.type === 'checkbox' ? target.checked : target.value;
        this.setState({
            [name]: value
        });
    }

    handelSubmit = (e) => {
        e.preventDefault();
        var {id, name, price, status } = this.state;
        var body = {
            id: id,
            name: name,
            price: price,
            status: status ? status : false
        };
        var { history } = this.props;
        if (this.state.id !== '') {
            this.props.handleUpdateProducts(body);
        }else{
            this.props.handleAddProducts(body);
        }
        history.goBack();
    }

    render() {

        var { name, price, status } = this.state;
        return (
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">

                <form method="POST" className="form-horizontal" onSubmit={this.handelSubmit}>
                    <div className="form-group">
                        <legend>Form title</legend>
                    </div>

                    <div className="form-group">
                        <label htmlFor="">Ten</label>
                        <input type="text" name="name" value={name} className="form-control" onChange={this.onChange} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="">Gia</label>
                        <input type="text" name="price" value={price} className="form-control" onChange={this.onChange} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="">Trang Thai</label>

                        <div className="checkbox">
                            <label>
                                <input checked={status} type="checkbox" name="status" value={status} onChange={this.onChange} />
                                Checkbox
                                </label>
                        </div>

                    </div>

                    <div className="form-group">
                        <button type="submit" className="btn btn-primary">Submit</button>
                    </div>
                </form>

            </div>
        );
    }
}

const MapStateToProps = state => {
    return {
        productItem: state.productItem
    }
};

const MapDispatchToProps = (dispatch, props) => {
    return {
        handleAddProducts: (body) => {
            dispatch(actionAddProductsRequest(body));
        },
        getProductItem: id => {
            dispatch(actionGetDetailProductRequest(id));
        },
        handleUpdateProducts: (body) => {
            dispatch(actionUpdateProductsRequest(body));
        }
        
    }
};

export default connect(MapStateToProps, MapDispatchToProps)(ProductActionPage);
