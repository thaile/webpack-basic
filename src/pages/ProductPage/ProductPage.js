import React, { Component } from 'react';
import {connect} from 'react-redux';
import ProductList from './../../components/Product/ProductList';
import ProductItem from './../../components/Product/ProductItem';
import {actionGetProductRequest, actionDeleteProductsRequest} from './../../actions/index';
import { Link } from 'react-router-dom';


class ProductPage extends Component {

    componentDidMount() {
        this.props.getProducts();
    }

    onDelete = id => {
        this.props.deleteProducts(id);
    }

    render() {
        var { products } = this.props;
        return (
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <Link to="/product/add" className="btn btn-primary">Thêm Mới</Link>
                <ProductList>
                    {this.showProductItem(products)}
                </ProductList>
            </div>
        );
    }

    showProductItem = products => {
        var result = null;
        if (products.length > 0) {
            result = products.map((product, index) => {
                return <ProductItem key={index} product={product} index={index} onDelete={this.onDelete} />
            });

        }
        return result;
    }
}

const MapStateToProps = state => {
    return {
        products: state.products
    }
};

const MapDispatchToProps = (dispatch, props) => {
    return {
        getProducts : () => {
            dispatch(actionGetProductRequest());
        },
        deleteProducts : (id) => {
            dispatch(actionDeleteProductsRequest(id));
        }
    }
};

export default connect(MapStateToProps, MapDispatchToProps)(ProductPage);
