import {combineReducers} from 'redux';
import products from './Product/products';
import productItem from './Product/productItem';


const appReducers = combineReducers({
    products,
    productItem
});

export default appReducers;