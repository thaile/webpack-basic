import * as types from './../../constants/ActionType';
import findIndexInArray from './../../utils/handleArray';

var initialState = [];

const products = (state=initialState, action) => {
    switch(action.type){
        case types.GET_PRODUCTS:
            state = action.products;
            return [...state];
        case types.REMOVE_PRODUCTS:
            var index = findIndexInArray(state, action.id);
            if(index !== -1){
                state.splice(index, 1);
            }
            return [...state];
        case types.ADD_PRODUCTS:
            state.push(action.products);
            return [...state];
        case types.UPDATE_PRODUCTS:
            var index = findIndexInArray(state, action.products.id);
            state[index] = action.products;
            return [...state];
        case types.GET_DETAIL_PRODUCTS:
            return [...state];
        default: return [...state];
    }
}

export default products;