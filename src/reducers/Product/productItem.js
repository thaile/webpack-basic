import * as types from './../../constants/ActionType';

var initialState = [];

const productItem = (state=initialState, action) => {
    switch(action.type){
        case types.GET_DETAIL_PRODUCTS:
            return action.products;
        default: return [...state];
    }
}

export default productItem;